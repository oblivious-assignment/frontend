## Steps to run the frontend application on local server

# VSCode way
1. Install `Live Server` extension of VSCode and run it.
2. This will start server at `http://localhost:5500/`.
3. Go to `http://localhost:5500/file-upload.html`.

# Http-server way
1. Open command prompt and run `npm i -g http-server`.
2. Go to project folder and open command prompt and run `http-server`.
3. This will start server at `http://127.0.0.1:8080/`.
4. Go to `http://127.0.0.1:8080/file-upload.html`.

## Make sure to install the extension and run the node server before running this application.
You can upload files here in this application which will be encrypted by the chrome extension on the go.
